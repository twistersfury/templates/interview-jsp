package com.twistersfury.interviewjsp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InterviewJspApplication {

    public static void main(String[] args) {
        SpringApplication.run(InterviewJspApplication.class, args);
    }

}
